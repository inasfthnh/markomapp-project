﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MA_DataModel;
using MA_ViewModel;

namespace MA_Repository
{
    public class t_EventRepo
    {
        //List
        public static List<t_EventView> All()
        {
            List<t_EventView> result = new List<t_EventView>();
            using (var db = new MAContext())
            {
                result = (from p in db.t_event
                          join e in db.m_employee
                          on p.assign_to equals e.id
                          where (p.is_delete == false)
                          select new t_EventView
                          {
                              id = p.id,
                              code = p.code,
                              event_name = p.event_name,
                              start_date = p.start_date,
                              end_date = p.end_date,
                              place = p.place,
                              budget = p.budget,
                              request_by = p.request_by,
                              request_date = p.request_date,
                              approved_by = p.approved_by,
                              approved_date = p.approved_date,
                              assign_to = p.assign_to,
                              closed_date = p.closed_date,
                              note = p.note,
                              status = p.status,
                              reject_reason = p.reject_reason,
                              created_by = p.created_by,
                              created_date = p.created_date,
                              assign_name = e.first_name
                          }).ToList();
            }
            return result;
        }

        public static Pagination Paging(int page, string searchCode, int searchRequest, string createBy, int perPage)
        {
            Pagination result = new Pagination();
            using (var db = new MAContext())
            {
                int rowCnt = db.t_event.Count(o => o.code.Contains(searchCode) && o.request_by == searchRequest && o.created_by.Contains(createBy) && o.is_delete == false);
                int pageCnt = rowCnt / perPage;
                if (rowCnt % perPage != 0)
                {
                    pageCnt++;
                }

                var query = (from p in db.t_event
                             where (p.is_delete == false && p.code.Contains(searchCode) &&
                                    p.request_by == searchRequest && 
                                    p.created_by.Contains(createBy))
                             select new t_EventView
                             {
                                 id = p.id,
                                 code = p.code,
                                 event_name = p.event_name,
                                 start_date = p.start_date,
                                 end_date = p.end_date,
                                 place = p.place,
                                 budget = p.budget,
                                 request_by = p.request_by,
                                 request_date = p.request_date,
                                 approved_by = p.approved_by,
                                 approved_date = p.approved_date,
                                 assign_to = p.assign_to,
                                 closed_date = p.closed_date,
                                 note = p.note,
                                 status = p.status,
                                 reject_reason = p.reject_reason,
                                 created_by = p.created_by,
                                 created_date = p.created_date
                             });

                query = query.OrderBy(o => o.code);
                query = query.Skip((page - 1) * perPage).Take(perPage);

                result.ListOf = query.ToList();
                result.Pages = pageCnt;
            }
            return result;
        }

        public static List<t_EventView> SearchDate(DateTime createDate, DateTime requestDate)
        {
            List<t_EventView> result = new List<t_EventView>();
            using (var db = new MAContext())
            { //hanya search tanggal yang masih error
                result = (from p in db.t_event
                          where (p.created_date.ToString("yyyy-MM-dd") == createDate.ToString("yyyy-MM-dd") && p.request_date.ToString("yyyy-MM-dd") == requestDate.ToString("yyyy-MM-dd") && p.is_delete == false)
                          select new t_EventView
                          {
                              id = p.id,
                              code = p.code,
                              event_name = p.event_name,
                              start_date = p.start_date,
                              end_date = p.end_date,
                              place = p.place,
                              budget = p.budget,
                              request_by = p.request_by,
                              request_date = p.request_date,
                              approved_by = p.approved_by,
                              approved_date = p.approved_date,
                              assign_to = p.assign_to,
                              closed_date = p.closed_date,
                              note = p.note,
                              status = p.status,
                              reject_reason = p.reject_reason,
                              created_by = p.created_by,
                              created_date = p.created_date
                          }).ToList();
            }
            return result;
        }

        public static ResponseResult Create(t_EventView entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new MAContext())
                {
                    t_event tevent = new t_event();
                    // Create
                    string newCode = GetNewCode();
                    entity.code = newCode;
                    tevent.code = entity.code;
                    tevent.event_name = entity.event_name;
                    tevent.start_date = entity.start_date;
                    tevent.end_date = entity.end_date;
                    tevent.place = entity.place;
                    tevent.budget = entity.budget;
                    tevent.request_by = 1;
                    tevent.request_date = DateTime.Now;
                    tevent.note = entity.note;
                    tevent.status = 1;
                    tevent.is_delete = false;
                    tevent.created_by = "Administrator";
                    tevent.created_date = DateTime.Now;

                    db.t_event.Add(tevent);
                    db.SaveChanges();

                    result.Message = "Berhasil dibuat";
                    result.Entity = entity;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }
            return result;
        }

        public static ResponseResult Edit(t_EventView entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new MAContext())
                {
                    t_event tevent = db.t_event
                        .Where(o => o.id == entity.id)
                        .FirstOrDefault();
                    if (tevent == null)
                    {
                        result.Success = false;
                    }
                    else
                    {
                        tevent.code = entity.code;
                        tevent.event_name = entity.event_name;
                        tevent.start_date = entity.start_date;
                        tevent.end_date = entity.end_date;
                        tevent.place = entity.place;
                        tevent.budget = entity.budget;
                        tevent.note = entity.note;
                        tevent.status = 1;
                        tevent.is_delete = false;
                        tevent.updated_by = "Administrator";
                        tevent.updated_date = DateTime.Now;
                        db.SaveChanges();

                        result.Message = "Berhasil diubah";
                        result.Entity = entity;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }
            return result;
        }

        public static ResponseResult Approval(t_EventView entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new MAContext())
                {
                    t_event tevent = db.t_event
                        .Where(o => o.id == entity.id)
                        .FirstOrDefault();
                    if (tevent == null)
                    {
                        result.Success = false;
                    }
                    else
                    {
                        if (entity.assign_to == null)
                        {
                            result.Success = false;
                        }
                        if (result.Success == true)
                        {
                            tevent.assign_to = entity.assign_to;
                            tevent.approved_by = 1;
                            tevent.approved_date = DateTime.Now;
                            tevent.status = 2;
                            db.SaveChanges();

                            result.Message = "Berhasil diapprove";
                            result.Entity = entity;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }
            return result;
        }

        public static ResponseResult Done(t_EventView entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new MAContext())
                {
                    t_event tevent = db.t_event
                        .Where(o => o.id == entity.id)
                        .FirstOrDefault();
                    if (tevent == null)
                    {
                        result.Success = false;
                    }
                    else
                    {
                        tevent.closed_date = DateTime.Now;
                        tevent.status = 3;
                        db.SaveChanges();

                        result.Message = "Event done";
                        result.Entity = entity;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }
            return result;
        }

        public static ResponseResult Reject(t_EventView entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new MAContext())
                {
                    t_event tevent = db.t_event
                        .Where(o => o.id == entity.id)
                        .FirstOrDefault();
                    if (tevent == null)
                    {
                        result.Success = false;
                    }
                    else
                    {
                        if (entity.reject_reason == null)
                        {
                            result.Success = false;
                        }
                        if (result.Success == true)
                        {
                            tevent.status = 0;
                            tevent.reject_reason = entity.reject_reason;
                            db.SaveChanges();

                            result.Message = "Berhasil direject";
                            result.Entity = entity;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }
            return result;
        }

        public static string GetNewCode()
        {
            string result = "TRWOEV" + DateTime.Now.ToString("ddMMyy") + "-";
            using (var db = new MAContext())
            {
                var maxCode = db.t_event
                    .Where(o => o.code.Contains(result))
                    .Select(o => new { codex = o.code })
                    .OrderByDescending(o => o.codex)
                    .FirstOrDefault();

                if (maxCode != null)
                {
                    string[] lastCode = maxCode.codex.Split('-');
                    int newInc = int.Parse(lastCode[1]) + 1;
                    result += newInc.ToString("D5");
                }
                else
                {
                    result += "00001";
                }
            }
            return result;
        }

        public static t_EventView ById(long id)
        {
            t_EventView result = new t_EventView();
            try
            {
                using (var db = new MAContext())
                {
                    result = (from p in db.t_event
                              where (p.id == id)
                              select new t_EventView
                              {
                                  id = p.id,
                                  code = p.code,
                                  start_date = p.start_date,
                                  end_date = p.end_date,
                                  place = p.place,
                                  budget = p.budget,
                                  request_by = p.request_by,
                                  request_date = p.request_date,
                                  approved_by = p.approved_by,
                                  approved_date = p.approved_date,
                                  assign_to = p.assign_to,
                                  closed_date = p.closed_date,
                                  note = p.note,
                                  status = p.status,
                                  reject_reason = p.reject_reason,
                                  created_by = p.created_by,
                                  created_date = p.created_date,
                                  event_name = p.event_name
                              }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return result == null ? new t_EventView() : result;
        }

        public static t_EventView DoneById(long id)
        {
            t_EventView result = new t_EventView();
            try
            {
                using (var db = new MAContext())
                {
                    result = (from p in db.t_event
                              join e in db.m_employee
                              on p.assign_to equals e.id
                              where (p.id == id)
                              select new t_EventView
                              {
                                  id = p.id,
                                  code = p.code,
                                  start_date = p.start_date,
                                  end_date = p.end_date,
                                  place = p.place,
                                  budget = p.budget,
                                  request_by = p.request_by,
                                  request_date = p.request_date,
                                  approved_by = p.approved_by,
                                  approved_date = p.approved_date,
                                  assign_to = p.assign_to,
                                  closed_date = p.closed_date,
                                  note = p.note,
                                  status = p.status,
                                  reject_reason = p.reject_reason,
                                  created_by = p.created_by,
                                  created_date = p.created_date,
                                  event_name = p.event_name,
                                  assign_name = e.first_name
                              }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return result == null ? new t_EventView() : result;
        }

    }
}