﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MA_DataModel;
using MA_ViewModel;

namespace MA_Repository
{
    public class m_EmployeeRepo
    {
        //List
        public static List<m_EmployeeView> All()
        {
            List<m_EmployeeView> result = new List<m_EmployeeView>();
            using (var db = new MAContext())
            {
                result = (from p in db.m_employee
                          join c in db.m_company
                          on p.m_company_id equals c.id
                          where (p.is_delete == false)
                          select new m_EmployeeView
                          {
                              id = p.id,
                              employee_number = p.employee_number,
                              first_name = p.first_name,
                              last_name = p.last_name,
                              m_company_id = p.m_company_id,
                              email = p.email,
                              created_by = p.created_by,
                              created_date = p.created_date,
                              company_name = c.name
                          }).ToList();
            }
            return result;
        }

        public static Pagination Paging(int page, string searchID, string searchName, string createBy, int perPage)
        {
            Pagination result = new Pagination();
            using (var db = new MAContext())
            {
                int rowCnt = db.m_employee.Count(o => o.employee_number.Contains(searchID) && (o.first_name.Contains(searchName) || o.last_name.Contains(searchName)) && o.created_by.Contains(createBy) && o.is_delete == false);
                int pageCnt = rowCnt / perPage;
                if (rowCnt % perPage != 0)
                {
                    pageCnt++;
                }
               
                var query = (from p in db.m_employee
                             join c in db.m_company
                             on p.m_company_id equals c.id
                             where (p.is_delete == false && p.employee_number.Contains(searchID) && (p.first_name.Contains(searchName) || p.last_name.Contains(searchName)) && p.created_by.Contains(createBy))
                             select new m_EmployeeView
                             {
                                 id = p.id,
                                 employee_number = p.employee_number,
                                 first_name = p.first_name,
                                 last_name = p.last_name,
                                 m_company_id = p.m_company_id,
                                 email = p.email,
                                 created_by = p.created_by,
                                 created_date = p.created_date,
                                 company_name = c.name
                             });

                query = query.OrderBy(o => o.employee_number);
                query = query.Skip((page - 1) * perPage).Take(perPage);

                result.ListOf = query.ToList();
                result.Pages = pageCnt;
            }
            return result;
        }

        public static List<m_EmployeeView> SearchDate(DateTime createDate)
        {
            List<m_EmployeeView> result = new List<m_EmployeeView>();
            using (var db = new MAContext())
            { //hanya search tanggal yang masih error
                result = (from p in db.m_employee
                          join c in db.m_company
                          on p.m_company_id equals c.id
                          where (p.created_date.ToString("yyyy-MM-dd") == createDate.ToString("yyyy-MM-dd") && p.is_delete == false)
                          orderby p.employee_number
                          select new m_EmployeeView
                          {
                              id = p.id,
                              employee_number = p.employee_number,
                              first_name = p.first_name,
                              last_name = p.last_name,
                              m_company_id = p.m_company_id,
                              email = p.email,
                              created_by = p.created_by,
                              created_date = p.created_date,
                              company_name = c.name
                          }).ToList();
            }
            return result;
        }

        public static ResponseResult Create(m_EmployeeView entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new MAContext())
                {
                    m_employee employee = new m_employee();
                    // Create
                    employee.employee_number = entity.employee_number;
                    employee.first_name = entity.first_name;
                    employee.last_name = entity.last_name;
                    employee.m_company_id = entity.m_company_id;
                    employee.email = entity.email;
                    employee.is_delete = false;
                    employee.created_by = "Administrator";
                    employee.created_date = DateTime.Now;
                    
                    db.m_employee.Add(employee);
                    db.SaveChanges();

                    result.empID = entity.employee_number;
                    result.Message = "Berhasil dibuat";
                    result.Entity = entity;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }
            return result;
        }

        public static ResponseResult Edit(m_EmployeeView entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new MAContext())
                {
                    m_employee employee = db.m_employee
                        .Where(o => o.id == entity.id)
                        .FirstOrDefault();
                    if (employee == null)
                    {
                        result.Success = false;
                    }
                    else
                    {
                        employee.employee_number = entity.employee_number;
                        employee.first_name = entity.first_name;
                        employee.last_name = entity.last_name;
                        employee.m_company_id = entity.m_company_id;
                        employee.email = entity.email;
                        employee.updated_by = "Administrator";
                        employee.updated_date = DateTime.Now;
                        db.SaveChanges();

                        result.Message = "Berhasil diubah";
                        result.Entity = entity;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }
            return result;
        }  

        public static m_EmployeeView ById(long id)
        {
            m_EmployeeView result = new m_EmployeeView();
            try
            {
                using (var db = new MAContext())
                {
                    result = (from p in db.m_employee
                              join c in db.m_company
                              on p.m_company_id equals c.id
                              where (p.id == id)
                              select new m_EmployeeView
                              {
                                  id = p.id,
                                  employee_number = p.employee_number,
                                  first_name = p.first_name,
                                  last_name = p.last_name,
                                  m_company_id = p.m_company_id,
                                  email = p.email,
                                  created_by = p.created_by,
                                  created_date = p.created_date,
                                  company_name = c.name
                              }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return result == null ? new m_EmployeeView() : result;
        }

        public static ResponseResult Delete(long id)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new MAContext())
                {
                    m_employee employee = db.m_employee
                        .Where(o => o.id == id)
                        .FirstOrDefault();

                    m_employee oldEmployee = employee;
                    result.Entity = oldEmployee;

                    if (employee != null)
                    {
                        employee.is_delete = true;

                        db.SaveChanges();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Employee not found!";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}