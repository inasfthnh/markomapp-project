﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MA_DataModel;
using MA_ViewModel;

namespace MA_Repository
{
    public class m_CompanyRepo
    {
        //List
        public static List<m_CompanyView> All()
        {
            List<m_CompanyView> result = new List<m_CompanyView>();
            using (var db = new MAContext())
            {
                result = (from p in db.m_company
                          where (p.is_delete == false)
                          select new m_CompanyView
                          {
                              id = p.id,
                              code = p.code,
                              name = p.name,
                              address = p.address,
                              phone = p.phone,
                              email = p.email,
                              created_by = p.created_by,
                              created_date = p.created_date
                          }).ToList();
            }
            return result;
        }

        public static Pagination Paging(int page, string searchCode, string searchName, int perPage, string createBy)
        {
            Pagination result = new Pagination();
            using (var db = new MAContext())
            {
                int rowCnt = db.m_company.Count(o => o.code.Contains(searchCode) && o.name.Contains(searchName) && o.created_by.Contains(createBy) && o.is_delete == false);
                int pageCnt = rowCnt / perPage;
                if (rowCnt % perPage != 0)
                {
                    pageCnt++;
                }

                var query = (from p in db.m_company
                             where (p.is_delete == false && p.code.Contains(searchCode) && p.name.Contains(searchName) && p.created_by.Contains(createBy))
                             select new m_CompanyView
                             {
                                 id = p.id,
                                 code = p.code,
                                 name = p.name,
                                 address = p.address,
                                 phone = p.phone,
                                 email = p.email,
                                 created_by = p.created_by,
                                 created_date = p.created_date
                             });

                query = query.OrderBy(o => o.code);
                query = query.Skip((page - 1) * perPage).Take(perPage);

                result.ListOf = query.ToList();
                result.Pages = pageCnt;
            }
            return result;
        }

        public static List<m_CompanyView> SearchDate(DateTime createDate)
        {
            List<m_CompanyView> result = new List<m_CompanyView>();
            using (var db = new MAContext())
            { //hanya search tanggal yang masih error
                result = (from p in db.m_company
                          where (p.created_date.ToString("yyyy-MM-dd") == createDate.ToString("yyyy-MM-dd") && p.is_delete == false)
                          orderby p.code
                          select new m_CompanyView
                          {
                                 id = p.id,
                                 code = p.code,
                                 name = p.name,
                                 address = p.address,
                                 phone = p.phone,
                                 email = p.email,
                                 created_by = p.created_by,
                                 created_date = p.created_date
                          }).ToList();
            }
            return result;
        }

        public static ResponseResult Create(m_CompanyView entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new MAContext())
                {
                    m_company company = new m_company();
                    // Create
                    string newCode = GetNewCode();
                    entity.code = newCode;
                    company.code = entity.code;
                    company.name = entity.name;
                    company.address = entity.address;
                    company.phone = entity.phone;
                    company.email = entity.email;
                    company.is_delete = false;
                    company.created_by = "Administrator";
                    company.created_date = DateTime.Now;

                    db.m_company.Add(company);
                    db.SaveChanges();

                    result.Message = "Berhasil dibuat";
                    result.Entity = entity;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }
            return result;
        }

        public static ResponseResult Edit(m_CompanyView entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new MAContext())
                {
                    m_company company = db.m_company
                        .Where(o => o.id == entity.id)
                        .FirstOrDefault();
                    if (company == null)
                    {
                        result.Success = false;
                    }
                    else
                    {
                        company.code = entity.code;
                        company.name = entity.name;
                        company.address = entity.address;
                        company.phone = entity.phone;
                        company.email = entity.email;
                        company.updated_by = "Administrator";
                        company.updated_date = DateTime.Now;
                        db.SaveChanges();

                        result.Message = "Berhasil diubah";
                        result.Entity = entity;
                    }                  
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }
            return result;
        }

        public static string GetNewCode()
        {
            string result = "CP-";
            using (var db = new MAContext())
            {
                var maxCode = db.m_company
                    .Where(o => o.code.Contains(result))
                    .Select(o => new { codex = o.code })
                    .OrderByDescending(o => o.codex)
                    .FirstOrDefault(); 

                if (maxCode != null)
                {
                    string[] lastCode = maxCode.codex.Split('-');
                    int newInc = int.Parse(lastCode[1]) + 1;
                    result += newInc.ToString("D4");
                }
                else
                {
                    result += "0001";
                }
            }
            return result;
        }

        public static m_CompanyView ById(long id)
        {
            m_CompanyView result = new m_CompanyView();
            try
            {
                using (var db = new MAContext())
                {
                    result = (from p in db.m_company
                              where (p.id == id)
                              select new m_CompanyView
                              {
                                  id = p.id,
                                  code = p.code,
                                  name = p.name,
                                  address = p.address,
                                  phone = p.phone,
                                  email = p.email,
                                  created_by = p.created_by,
                                  created_date = p.created_date
                              }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return result == null ? new m_CompanyView() : result;
        }

        public static ResponseResult Delete(long id)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new MAContext())
                {
                    m_company company = db.m_company
                        .Where(o => o.id == id)
                        .FirstOrDefault();

                    m_company oldCompany = company;
                    result.Entity = oldCompany;

                    if (company != null)
                    {
                        company.is_delete = true;
                        //result.Code = company.code;
                        db.SaveChanges();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Company not found!";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}