﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MA_DataModel;
using MA_ViewModel;

namespace MA_Repository
{
    public class m_RoleRepo
    {
        //List
        public static List<m_RoleView> All()
        {
            List<m_RoleView> result = new List<m_RoleView>();
            using (var db = new MAContext())
            {
                result = (from p in db.m_role
                          where (p.is_delete == false)
                          select new m_RoleView
                          {
                              id = p.id,
                              code = p.code,
                              name = p.name,
                              description = p.description,
                              created_by = p.created_by,
                              created_date = p.created_date
                          }).ToList();
            }
            return result;
        }

        public static Pagination Paging(int page, string searchCode, string searchName, int perPage, string createBy)
        {
            Pagination result = new Pagination();
            using (var db = new MAContext())
            {
                int rowCnt = db.m_role.Count(o => o.code.Contains(searchCode) && o.name.Contains(searchName) && o.created_by.Contains(createBy) && o.is_delete == false);
                int pageCnt = rowCnt / perPage;
                if (rowCnt % perPage != 0)
                {
                    pageCnt++;
                }

                var query = (from p in db.m_role
                             where (p.is_delete == false && p.code.Contains(searchCode) && p.name.Contains(searchName) && p.created_by.Contains(createBy))
                             select new m_RoleView
                             {
                                 id = p.id,
                                 code = p.code,
                                 name = p.name,
                                 description = p.description,
                                 created_by = p.created_by,
                                 created_date = p.created_date
                             });

                query = query.OrderBy(o => o.code);
                query = query.Skip((page - 1) * perPage).Take(perPage);

                result.ListOf = query.ToList();
                result.Pages = pageCnt;

                //m_RoleView delCode = new m_RoleView();
                //result.DelCode = delCode.code;
            }
            return result;
        }

        public static List<m_RoleView> SearchDate(DateTime createDate)
        {
            List<m_RoleView> result = new List<m_RoleView>();
            using (var db = new MAContext())
            { //hanya search tanggal yang masih error
                result = (from p in db.m_role
                          where (p.created_date.ToString("yyyy-MM-dd") == createDate.ToString("yyyy-MM-dd") && p.is_delete == false)
                          orderby p.code
                          select new m_RoleView
                          {
                              id = p.id,
                              code = p.code,
                              name = p.name,
                              description = p.description,
                              created_by = p.created_by,
                              created_date = p.created_date
                          }).ToList();
            }
            return result;
        }

        public static ResponseResult Create(m_RoleView entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new MAContext())
                {
                    m_role role = new m_role();
                    // Create
                    string newCode = GetNewCode();
                    entity.code = newCode;
                    role.code = entity.code;
                    role.name = entity.name;
                    role.description = entity.description;
                    role.is_delete = false;
                    role.created_by = "Administrator";
                    role.created_date = DateTime.Now;

                    db.m_role.Add(role);
                    db.SaveChanges();

                    result.Message = "Berhasil dibuat";
                    result.Entity = entity;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }
            return result;
        }

        public static ResponseResult Edit(m_RoleView entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new MAContext())
                {
                    m_role role = db.m_role
                        .Where(o => o.id == entity.id)
                        .FirstOrDefault();
                    if (role == null)
                    {
                        result.Success = false;
                    }
                    else
                    {
                        role.code = entity.code;
                        role.name = entity.name;
                        role.description = entity.description;
                        role.updated_by = "Administrator";
                        role.updated_date = DateTime.Now;
                        db.SaveChanges();

                        result.Message = "Berhasil diubah";
                        result.Entity = entity;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }
            return result;
        }

        public static string GetNewCode()
        {
            string result = "RO-";
            using (var db = new MAContext())
            {
                var maxCode = db.m_role
                    .Where(o => o.code.Contains(result))
                    .Select(o => new { codex = o.code })
                    .OrderByDescending(o => o.codex)
                    .FirstOrDefault();

                if (maxCode != null)
                {
                    string[] lastCode = maxCode.codex.Split('-');
                    int newInc = int.Parse(lastCode[1]) + 1;
                    result += newInc.ToString("D4");
                }
                else
                {
                    result += "0001";
                }
            }
            return result;
        }

        public static m_RoleView ById(long id)
        {
            m_RoleView result = new m_RoleView();
            try
            {
                using (var db = new MAContext())
                {
                    result = (from p in db.m_role
                              where (p.id == id)
                              select new m_RoleView
                              {
                                  id = p.id,
                                  code = p.code,
                                  name = p.name,
                                  description = p.description,
                                  created_by = p.created_by,
                                  created_date = p.created_date
                              }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return result == null ? new m_RoleView() : result;
        }

        public static ResponseResult Delete(long id)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new MAContext())
                {
                    m_role role = db.m_role
                        .Where(o => o.id == id)
                        .FirstOrDefault();

                    m_role oldRole = role;
                    result.Entity = oldRole;

                    if (role != null)
                    {
                        role.is_delete = true;
                        result.DelCode = role.code;
                        db.SaveChanges();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Role not found!";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}