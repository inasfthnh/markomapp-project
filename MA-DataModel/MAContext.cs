namespace MA_DataModel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class MAContext : DbContext
    {
        public MAContext()
            : base("name=MAContext")
        {
        }

        public virtual DbSet<m_company> m_company { get; set; }
        public virtual DbSet<m_employee> m_employee { get; set; }
        public virtual DbSet<m_role> m_role { get; set; }
        public virtual DbSet<t_event> t_event { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<m_company>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<m_company>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<m_company>()
                .Property(e => e.address)
                .IsUnicode(false);

            modelBuilder.Entity<m_company>()
                .Property(e => e.phone)
                .IsUnicode(false);

            modelBuilder.Entity<m_company>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<m_company>()
                .Property(e => e.created_by)
                .IsUnicode(false);

            modelBuilder.Entity<m_company>()
                .Property(e => e.updated_by)
                .IsUnicode(false);

            modelBuilder.Entity<m_company>()
                .HasMany(e => e.m_employee)
                .WithOptional(e => e.m_company)
                .HasForeignKey(e => e.m_company_id);

            modelBuilder.Entity<m_employee>()
                .Property(e => e.employee_number)
                .IsUnicode(false);

            modelBuilder.Entity<m_employee>()
                .Property(e => e.first_name)
                .IsUnicode(false);

            modelBuilder.Entity<m_employee>()
                .Property(e => e.last_name)
                .IsUnicode(false);

            modelBuilder.Entity<m_employee>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<m_employee>()
                .Property(e => e.created_by)
                .IsUnicode(false);

            modelBuilder.Entity<m_employee>()
                .Property(e => e.updated_by)
                .IsUnicode(false);

            modelBuilder.Entity<m_employee>()
                .HasMany(e => e.t_event)
                .WithOptional(e => e.m_employee)
                .HasForeignKey(e => e.approved_by);

            modelBuilder.Entity<m_employee>()
                .HasMany(e => e.t_event1)
                .WithOptional(e => e.m_employee1)
                .HasForeignKey(e => e.assign_to);

            modelBuilder.Entity<m_employee>()
                .HasMany(e => e.t_event2)
                .WithRequired(e => e.m_employee2)
                .HasForeignKey(e => e.request_by)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<m_role>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<m_role>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<m_role>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<m_role>()
                .Property(e => e.created_by)
                .IsUnicode(false);

            modelBuilder.Entity<m_role>()
                .Property(e => e.updated_by)
                .IsUnicode(false);

            modelBuilder.Entity<t_event>()
                .Property(e => e.code)
                .IsUnicode(false);

            modelBuilder.Entity<t_event>()
                .Property(e => e.event_name)
                .IsUnicode(false);

            modelBuilder.Entity<t_event>()
                .Property(e => e.place)
                .IsUnicode(false);

            modelBuilder.Entity<t_event>()
                .Property(e => e.note)
                .IsUnicode(false);

            modelBuilder.Entity<t_event>()
                .Property(e => e.reject_reason)
                .IsUnicode(false);

            modelBuilder.Entity<t_event>()
                .Property(e => e.created_by)
                .IsUnicode(false);

            modelBuilder.Entity<t_event>()
                .Property(e => e.updated_by)
                .IsUnicode(false);
        }
    }
}
