﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_DataModel
{
    public class Pagination
    {
        public Pagination()
        {
            Pages = 0;
        }
        //Total page
        public int Pages { get; set; }
        public object ListOf { get; set; }

        public string Code { get; set; }
        public string DelCode { get; set; }
    }
}
