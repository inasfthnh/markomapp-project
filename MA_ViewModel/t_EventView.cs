﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace MA_ViewModel
{
    public class t_EventView
    {
        public int id { get; set; }

        [Required]
        [StringLength(50)]
        public string code { get; set; }

        [Required(ErrorMessage = "Kolom Event Name harus diisi.")]
        [StringLength(255)]
        public string event_name { get; set; }

        [DataType(DataType.Date), Required(ErrorMessage = "Kolom Start Date harus diisi.")]
        [Display(Name = "Start Date"), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? start_date { get; set; }

        [DataType(DataType.Date), Required(ErrorMessage = "Kolom End Date harus diisi.")]
        [Display(Name = "End Date"), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? end_date { get; set; }

        [StringLength(255)]
        public string place { get; set; }
      
        public long? budget { get; set; }

        public int request_by { get; set; }

        public DateTime request_date { get; set; }

        public string requestdate
        {
            get { return request_date.ToString("dd/MM/yyyy"); }
        }

        public int? approved_by { get; set; }

        public DateTime? approved_date { get; set; }

        [Required(ErrorMessage = "Kolom Assign To harus diisi.")]
        public int? assign_to { get; set; }
        public string assign_name { get; set; }

        public DateTime? closed_date { get; set; }

        [StringLength(255)]
        public string note { get; set; }

        public int? status { get; set; }

        public string status_name {
            get
            {
                if (status == 1)
                { return "Submitted"; }
                else if (status == 2)
                { return "In Progress"; }
                else if (status == 3)
                { return "Done"; }
                else if (status == 0)
                { return "Rejected"; }
                else { return ""; }
            }
        }

        [Required(ErrorMessage = "Kolom Reject Reason harus diisi.")]
        [StringLength(255)]
        public string reject_reason { get; set; }

        [StringLength(50)]
        public string created_by { get; set; }

        public DateTime created_date { get; set; }

        public string createdate
        {
            get { return created_date.ToString("dd/MM/yyyy"); }
        }
    }
}

