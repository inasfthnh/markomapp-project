﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace MA_ViewModel
{
    public class m_EmployeeView
    {
        public int id { get; set; }
        public int? ids { get { return id; } }

        [Required]
        [StringLength(50)]
        public string employee_number { get; set; }

        [Required]
        [StringLength(50)]
        public string first_name { get; set; }

        [StringLength(50)]
        public string last_name { get; set; }

        public string full_name 
        { 
            get { return string.Format("{0} {1}", first_name, last_name); }
        }

        public int? m_company_id { get; set; }

        public string company_name { get; set; }

        [StringLength(150)]
        public string email { get; set; }

        [Required]
        [StringLength(50)]
        public string created_by { get; set; }

        public DateTime created_date { get; set; }

        public string createdate
        {
            get { return created_date.ToString("dd/MM/yyyy"); }
        }

    }
}

