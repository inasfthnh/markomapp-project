﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MA_DataModel;
using MA_ViewModel;
using MA_Repository;

namespace MarkomApp.Controllers
{
    public class m_EmployeeController : Controller
    {
        // GET: Employee
        public ActionResult Index()
        {
            ViewBag.CompanyName = new SelectList(m_CompanyRepo.All(), "id", "name");
            return View();
        }
 
        public ActionResult List(int page, string searchID, string searchName, string createBy, int perPage)
        {
            Pagination pg = m_EmployeeRepo.Paging(page, searchID, searchName, createBy, perPage);
            ViewBag.currPage = page;
            ViewBag.PageCount = pg.Pages;
            return PartialView("_List", pg.ListOf);
        }

        public ActionResult SearchDate(DateTime createDate)
        {
            return PartialView("_List", m_EmployeeRepo.SearchDate(createDate));
        }

        //Get
        public ActionResult Create()
        {
            ViewBag.CompanyList = new SelectList(m_CompanyRepo.All(), "id", "name");
            return PartialView("_Create");
        }

        [HttpPost]
        public ActionResult Create(m_EmployeeView model)
        {
            ResponseResult result = m_EmployeeRepo.Create(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message,
                    entity = result.Entity
                },
                JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detail(long id)
        {
            return PartialView("_Detail", m_EmployeeRepo.ById(id));
        }

        //Get
        public ActionResult Edit(long id)
        {
            ViewBag.CompanyList = new SelectList(m_CompanyRepo.All(), "id", "name");
            return PartialView("_Edit", m_EmployeeRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Edit(m_EmployeeView model)
        {
            ResponseResult result = m_EmployeeRepo.Edit(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message
                },
                JsonRequestBehavior.AllowGet);
        }

        //Get
        public ActionResult Delete(long id)
        {
            return PartialView("_Delete", m_EmployeeRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Delete(m_employee model)
        {
            ResponseResult result = m_EmployeeRepo.Delete(model.id);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message
                },
                JsonRequestBehavior.AllowGet);

        }

    }
}