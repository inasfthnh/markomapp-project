﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MA_DataModel;
using MA_ViewModel;
using MA_Repository;

namespace MarkomApp.Controllers
{
    public class m_CompanyController : Controller
    {
        // GET: Company
        public ActionResult Index()
        {
            ViewBag.CompanyCode = new SelectList(m_CompanyRepo.All(), "code", "code");
            ViewBag.CompanyName = new SelectList(m_CompanyRepo.All(), "name", "name");
            ViewBag.Code = m_CompanyRepo.GetNewCode();
            return View();
        }

        public ActionResult List(int page, string searchCode, string searchName, int perPage, string createBy)
        {
            Pagination pg = m_CompanyRepo.Paging(page, searchCode, searchName, perPage, createBy);
            ViewBag.currPage = page;
            ViewBag.PageCount = pg.Pages;

            return PartialView("_List", pg.ListOf);
        }

        public ActionResult SearchDate(DateTime createDate)
        {
            return PartialView("_List", m_CompanyRepo.SearchDate(createDate));
        }

        //Get
        public ActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public ActionResult Create(m_CompanyView model)
        {
            ResponseResult result = m_CompanyRepo.Create(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message,
                    entity = result.Entity
                },
                JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detail(long id)
        {
            return PartialView("_Detail", m_CompanyRepo.ById(id));
        }

        //Get
        public ActionResult Edit(long id)
        {
            return PartialView("_Edit", m_CompanyRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Edit(m_CompanyView model)
        {
            ResponseResult result = m_CompanyRepo.Edit(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message
                },
                JsonRequestBehavior.AllowGet);
        }

        //Get
        public ActionResult Delete(long id)
        {
            return PartialView("_Delete", m_CompanyRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Delete(m_company model)
        {
            ResponseResult result = m_CompanyRepo.Delete(model.id);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message
                },
                JsonRequestBehavior.AllowGet);

        }

    }
}