﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MA_DataModel;
using MA_ViewModel;
using MA_Repository;

namespace MarkomApp.Controllers
{
    public class t_EventController : Controller
    {
        // GET: Event
        public ActionResult Index()
        {
            ViewBag.Code = t_EventRepo.GetNewCode();
            return View();
        }

        public ActionResult List(int page, string searchCode, int searchRequest, string createBy, int perPage)
        {
            Pagination pg = t_EventRepo.Paging(page, searchCode, searchRequest, createBy, perPage);
            ViewBag.currPage = page;
            ViewBag.PageCount = pg.Pages;
            return PartialView("_List", pg.ListOf);
        }

        public ActionResult SearchDate(DateTime createDate, DateTime requestDate)
        {
            return PartialView("_List", t_EventRepo.SearchDate(createDate, requestDate));
        }

        //Get
        public ActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public ActionResult Create(t_EventView model)
        {
            ResponseResult result = t_EventRepo.Create(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message,
                    entity = result.Entity
                },
                JsonRequestBehavior.AllowGet);
        }

        //Get
        public ActionResult Edit(long id)
        {
            return PartialView("_Edit", t_EventRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Edit(t_EventView model)
        {
            ResponseResult result = t_EventRepo.Edit(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message
                },
                JsonRequestBehavior.AllowGet);
        }

        public ActionResult Approval(long id)
        {
            ViewBag.EmployeeList = new SelectList(m_EmployeeRepo.All(), "id", "full_name");
            return PartialView("_Approval", t_EventRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Approval(t_EventView model)
        {
            ResponseResult result = t_EventRepo.Approval(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message
                },
                JsonRequestBehavior.AllowGet);
        }

        public ActionResult Done(long id)
        {
            return PartialView("_Done", t_EventRepo.DoneById(id));
        }

        [HttpPost]
        public ActionResult Done(t_EventView model)
        {
            ResponseResult result = t_EventRepo.Done(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message
                },
                JsonRequestBehavior.AllowGet);
        }

        public ActionResult Reject(long id)
        {
            return PartialView("_Reject", t_EventRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Reject(t_EventView model)
        {
            ResponseResult result = t_EventRepo.Reject(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message
                },
                JsonRequestBehavior.AllowGet);
        }
    }
}