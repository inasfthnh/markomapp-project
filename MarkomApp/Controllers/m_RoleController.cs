﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MA_DataModel;
using MA_ViewModel;
using MA_Repository;

namespace MarkomApp.Controllers
{
    public class m_RoleController : Controller
    {
        // GET: Role
        public ActionResult Index()
        {
            ViewBag.RoleCode = new SelectList(m_RoleRepo.All(), "code", "code");
            ViewBag.RoleName = new SelectList(m_RoleRepo.All(), "name", "name");
            ViewBag.Code = m_RoleRepo.GetNewCode();
            return View();
        }

        public ActionResult List(int page, string searchCode, string searchName, int perPage, string createBy)
        {
            Pagination pg = m_RoleRepo.Paging(page, searchCode, searchName, perPage, createBy);
            ViewBag.currPage = page;
            ViewBag.PageCount = pg.Pages;
            return PartialView("_List", pg.ListOf);
        }

        public ActionResult SearchDate(DateTime createDate)
        {
            return PartialView("_List", m_RoleRepo.SearchDate(createDate));
        }

        //Get
        public ActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public ActionResult Create(m_RoleView model)
        {
            ResponseResult result = m_RoleRepo.Create(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message,
                    entity = result.Entity
                },
                JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detail(long id)
        {
            return PartialView("_Detail", m_RoleRepo.ById(id));
        }

        //Get
        public ActionResult Edit(long id)
        {
            return PartialView("_Edit", m_RoleRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Edit(m_RoleView model)
        {
            ResponseResult result = m_RoleRepo.Edit(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message
                },
                JsonRequestBehavior.AllowGet);
        }

        //Get
        public ActionResult Delete(long id)
        {
            return PartialView("_Delete", m_RoleRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Delete(m_role model)
        {
            ResponseResult result = m_RoleRepo.Delete(model.id);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message
                },
                JsonRequestBehavior.AllowGet);

        }

    }
}